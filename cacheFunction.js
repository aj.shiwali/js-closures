function cacheFunction(cb){
    const obj = new Set();
    function returnFunction(value){
        if(obj.size>0){
           return obj; 
        }
        else{
            obj.add(value);
            console.log(obj);
            return cb(value);
        }
    }
    return returnFunction;
}
module.exports = cacheFunction;
