function counterFactory(){
    var incrementDercrement = (function(){
        return{
            increment: function(counter){
                return counter+1;
            },
            decrement: function(counter){
                return counter-1;
            }
        };
    })();
    return incrementDercrement;
}
module.exports = counterFactory;